export default (config) => {
  config.timelapseLookBackPerfRun = 500;
  config.createTimelapse = true;

  return config;
};